All,

After a great many iterations and revisions, I submit for your consideration my automated Exchange Online Configuration tool for Proofpoint integrations in the US-3 zone. If your integration is outside of the US3 zone, you will have to manually modify the addresses containing US3 as well as the IP address list for the inbound connector.

I am working to make this as broadly compatible as possible, but only have testing capability for the tenants I have in the zone I'm in. This is an open effort and any input is always appreciated. Comments can be sent to ($mygitlabusername@gmail.com).

To use the tool please pull the repo from my gitlab at https://gitlab.com/kyleclarktech/o365-PP-config
 
Previous iterations required several scripts controlled by a .bat file, but it has been compiled into one large powershell script that should run all of the components in sequence.
 
When run, it will check to see that your computer meets requirements for creating a session with exchange online. 
After validating the computer meets requirements it will prompt you for credentials for the client’s tenant level admin account. 
After collecting those it checks for and removes any lingering powershell connections to exchange, before creating a new session with Exchange Online. 

It then walks you through the following Branching options:
Do you want to create an inbound connector? [Yes/ No] If Yes >Should it be turned on? [Yes/No]
Do you want to create an outbound connector? [Yes/No ] If Yes >Should it be turned on? [Yes/No] If Yes> Turn on and proceed to validate connector.
Do you want to create Spam Filter Bypass rule for Proofpoint? [Yes/No]

At each stage it checks for errors. If it finds them it should alert the user before automatically disconnecting the session and providing basic troubleshooting steps to follow. The tool can complete all of these steps in about 60 seconds. 

No longer will we need to manually enter IP addresses line by line due to Microsoft’s inability to allow easy data entry.

Anyone that’s configuring new clients for Proofpoint integration PLEASE give it a shot and let me know if there is anything that could be improved.

Tell me what you guys think.

Any input is appreciated!

Thanks,
Kyle
